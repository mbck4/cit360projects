public class avengers {

    private String avenger;
    private String movie;

    public avengers(String avenger, String movie) {
        this.avenger = avenger;
        this.movie = movie;
    }

    public String toString() {
        return "avenger: " + avenger + " movie: " + movie;
    }
}
