import java.util.ArrayList;

public class collections {
  public static void main(String[] args) {
    ArrayList<String> avengers = new ArrayList<String>();
    avengers.add("Thor");
    avengers.add("Iron Man");
    avengers.add("Black Widow");
    avengers.add("Captain America");
    System.out.println(avengers);
  }

  

 public static void main(String[] args) {
    HashSet<String> avengers = new HashSet<String>();
    avengers.add("Thor");
    avengers.add("Iron Man");
    avengers.add("Black Widow");
    avengers.add("Captain America");
    System.out.println(avengers);
  }

  System.out.println("-- List using Generics --");
  List<avengers> myList = new LinkedList<avengers>();
  avengers.add(new avengers("Thor, "Thor "));
  avengers.add(new avengers("Iron Man", "Avengers 2"));
  avengers.add(new avengers("Black Widow", "Avengers 1"));
  avengers.add(new avengers("Captain America", "Captain America 2"));

  for (avengers avenger : myList) {
      System.out.println(avenger);
  }

}


